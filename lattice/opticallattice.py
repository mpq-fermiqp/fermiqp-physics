from re import L
import numpy as np
from math import pi
import warnings
import matplotlib.pyplot as plt


### Definition of natural constants ###
hplanck = 6.63e-34
hbar = hplanck / 2 / pi
mass = 6 * 1.67e-27
kB = 1.38e-23
epsilon0 = 8.8541878128e-12
c = 299792458
Gamma = 2 * pi * 5.8724e6
omega0 = 2 * pi * 446.799677e12


### Conversion functions ###
def temp2freq(T):
    """
    Converts temperature values from Kelvin to frequency in Hz
    """
    return kB * T / hplanck


def freq2temp(freq):
    """
    Converts frequency values from Herz into Kelvin
    """
    return hplanck * freq / kB


def power2freq(power=1.0, waist=60e-6, waisty=None, wavelen=1.064e-6):
    """Calculates the trap depth U in units of Hz for a TLS created by a single beam.
    For a lattice this number needs to be multiplied by an eta_boost parameter that
    takes into account the intenstiy boost from the interference
    Example: 1) For two beams interfering eta_boost = (1+1)**2 = 4
             2) For our pinning lattice the power ratio goes down by a factor of t=0.91
                for every pass through the glass cell. Hence the correct boost factor is

                eta_boost = (t + t**3 + t**5 + t**7)**2

                Note that in this definition we include losses on the first entry on the glass cell.
                Therefore we can plug in the beam power that we measure in the experiment just before
                it enters the glass cell.

    Parameters
    ========
    power       Power of the beam in W
    waist       Guassian beam waist in m
    waisty      For elliptical beams (optional), secondary Guassian beam waist in m
    wavelen     wavelength in m

    Returns
    ========
    U:           Trap depth in units of Hz (i.e. trap depth U in Joule divided by Planck constant h)

    TODOs
    ========
    implement calculation for alkali (see qcalculate from Li1.0)
    """
    if waisty is None:
        waisty = waist
    Imax = 2 * power / (pi * waist * waisty)
    omega = 2 * pi * c / wavelen
    U = -(
        3
        * pi
        * c**2
        / 2
        / omega0**3
        * Gamma
        * (1 / (omega0 - omega) + 1 / (omega0 + omega))
        * Imax
    )
    return U / hplanck


### Class definitions fo Dipole traps and lattices ###
class DipoleTrap:
    """Optical dipole trap created by a single beam

    Parameters
    =========
    waist       Gaussian beam waist in m
    waisty      For elliptical beams (optional), secondary Guassian beam waist in m
    wavelen     Wavelength in m
    U           Trap depth in Hz

    """

    def __init__(
        self,
        waist=60e-6,
        waisty=None,
        wavelen=1.064e-6,
        U=1,
    ) -> None:
        self.waist = waist
        self.wavelen = wavelen
        self.zR = pi * self.waist**2 / self.wavelen
        self.U = U

        if waisty:
            self.waisty = waisty
            self.zRy = pi * self.waisty**2 / self.wavelen

    def trap_freq(self, U=None, unit="Hz", axis="radial"):
        """Reuturns trap frequencies for a dipole trap instance

        Parameters
        =========
        U           Trap depth in Hz
        unit        optional, accepted values are "Hz" and "K"
                    Default is "Hz"
        axis        optional, accepted values are "axial" and "radial"

        Returns
        ========
        f           trap frequency in Hz.
                    For elliptical beams, returns a list of two trap frequencies if axis
                    is "radial"
        """
        if U is None:
            U = self.U

        if unit == "Hz":
            pass
        elif unit == "K":
            U = temp2freq(U)

        if axis == "radial":
            try:
                self.waisty
            except Exception:
                frequency = (4 * U * hplanck / mass / self.waist**2) ** 0.5 / 2 / pi
            else:
                frequency = [
                    (4 * U * hplanck / mass / self.waist**2) ** 0.5 / 2 / pi,
                    (4 * U * hplanck / mass / self.waisty**2) ** 0.5 / 2 / pi,
                ]
        elif axis == "axial":
            try:
                self.waisty
            except Exception:
                frequency = (2 * U * hplanck / mass / self.zR**2) ** 0.5 / 2 / pi
            else:
                # Use expression derived in Lucas mathematice notebook
                wz0 = (2 * pi * self.waist**2 * self.waisty**2) / (
                    self.wavelen * np.sqrt(self.waist**4 + self.waisty**4)
                )
                frequency = (4 * U * hplanck / mass / wz0**2) ** 0.5 / 2 / pi
        return frequency

    def a_ho(self, U=None):
        """Returns the size of the ground state of the harmonic oscillator in
        the axial and radial directions.

        Returns
        =====
        a_ho        List with 2 entries for circular trap, and 3 entries for
                    an elliptical trap. unit (m)
        """
        if U is None:
            U = self.U
        try:
            self.waisty
        except Exception:
            a_ho = [
                np.sqrt(hbar / mass / self.trap_freq(U=U, axis="radial") / 2 / pi),
                np.sqrt(hbar / mass / self.trap_freq(U=U, axis="axial") / 2 / pi),
            ]
        else:
            a_ho = [
                np.sqrt(hbar / mass / self.trap_freq(U=U, axis="radial")[0] / 2 / pi),
                np.sqrt(hbar / mass / self.trap_freq(U=U, axis="radial")[1] / 2 / pi),
                np.sqrt(hbar / mass / self.trap_freq(U=U, axis="axial") / 2 / pi),
            ]

        return a_ho


class OpticalLattice1D:
    """Optical one-dimensional lattice with known spacing and trap depth

    Parameters
    =========
    a       lattice spacing in m
    U       trap depth in units of

    Properties
    =========
    Er      recoil energy in units of Hz

    """

    def __init__(self, a=1.1e-6, U=1) -> None:
        self.a = a
        self.Er = hplanck / 8 / mass / (self.a) ** 2
        self.U = U

    def t(self, U=None):
        """Returns tunneling rate in units of Hz
        note: only valid for deep lattices, error smaller 10% if U > 15Er
        """
        if U is None:
            U_in_Er = self.U / self.Er
        else:
            U_in_Er = U / self.Er
        return (
            4
            / np.pi**0.5
            * (U_in_Er) ** (3 / 4)
            * np.exp(-2 * (U_in_Er) ** 0.5)
            * self.Er
        )

    def trap_freq(self, U=None, unit="Hz"):
        if U is None:
            U = self.U

        if unit == "Hz":
            pass
        elif unit == "K":
            U = temp2freq(U)

        return 1 / 2 / self.a * np.sqrt(2 * U * hplanck / mass)

    def exactharmonics(self, n=0, U=None):
        if U is None:
            U = self.U

        pass

    def a_ho(self, U=None):
        if U is None:
            U = self.U

        return np.sqrt(hbar / mass / self.trap_freq(U=U) / 2 / pi)


class OpticalLatticeAngle:
    """Optical lattice created by two beams interfering under an angle

    Parameters
    =========
    waist       Gaussian beam waist in m
    waisty      For elliptical beams (optional), secondary Guassian beam waist in m
    half_angle  Half the interference angle between the
                two beams creating the lattice in degrees
    power       The power in each individual beam in W
    wavelen     Wavelength in m
    U           depth in Hz

    """

    def __init__(
        self,
        waist=60e-6,
        waisty=None,
        half_angle=10,
        power=1,
        wavelen=1.064e-6,
        # U=1,
    ):
        if waisty is not None:
            self.waisty = waisty
        else:
            self.waisty = waist

        self.waist = waist
        self.half_angle = half_angle
        self.power = power
        self.wavelen = wavelen
        self.a = self.wavelen / (2 * np.sin(np.pi / 180 * self.half_angle))
        self.Er = hplanck / 8 / mass / (self.a) ** 2  # Hz
        # self.U = Udd
        self.U = (
            power2freq(
                self.power,
                self.waist,
                self.waisty,
                self.wavelen,
            )
            * 4
        )  # Hz

        self.U_abs = np.abs(
            self.U
        )  # useful for blue detuned lattices, where the trapping is negative

        self.U_in_Er = self.U / self.Er

    def t(self, U=None):
        """Returns tunneling rate in units of Hz

        Parameters
        ----
        U   optional, in Hz

        note: only valid for deep lattices, error smaller 10% if U > 15Er
        """
        if U is None:
            U_in_Er = np.abs(self.U_in_Er)
        else:
            U_in_Er = np.abs(U / self.Er)
        return (
            4
            / np.pi**0.5
            * (U_in_Er) ** (3 / 4)
            * np.exp(-2 * (U_in_Er) ** 0.5)
            * self.Er
        )

    def on_site_freq(self, U=None, unit="Hz"):
        """Returns on site frequency
        Parameters:


        """

        if U is None:
            U = self.U_abs

        if unit == "Hz":
            pass
        elif unit == "K":
            U = temp2freq(U)

        return 2 * np.sqrt(U / self.Er) * self.Er

    def a_ho(self, U=None):
        if U is None:
            U = self.U

        return np.sqrt(hbar / mass / self.on_site_freq(U=U) / 2 / pi)

    def __str__(self):
        return f"Lattice with interference half-angle {self.half_angle}"

    def print_params(self):
        """Does the same as vars() but prettier"""

        print(f"Power per beam: {self.power:.1f} W")
        print(f"Waists: {self.waist /1e-6:.3f}, {self.waisty/1e-6:.3f} um")
        print(f"Lattice spacing: {self.a/1e-6:.3f} um")
        print(f"Recoil energy: {self.Er:.3f} Hz")
        print(
            f"Depth: {self.U /1e3:.3f} kHz, {self.U_in_Er:.3f} Er, {freq2temp(self.U)/1e-6:.3f} uK"
        )
        print(
            f"On site: {self.on_site_freq() /1e3:.3f} kHz, {freq2temp(self.on_site_freq())/1e-6:.3f} uK"
        )


class GaussianBeam3D:
    """Gaussian Beams in 3D space

    Parameters:
        waist:       Gaussian beam waist in m
        focuspos:    Positon of the focus
        wavelen:     Wavelength in m
        prop_dir:    Propagation direction
        polar_dir:   Polarization direction (linear)
        phi:         Arbitrary Phase in rad. NOTE: this translates in a 2*phi when
                        calculating the intensity
        power:       Optional, The power in each individual beam in W
        depth:       Optional, Depth of the dipole trap crated by the beam in Hz.
                        NOTE: There are no checks on physical meanings of values





    TODO:
        Implement elliptical beams
        Scale E field with power
        Check all phase and wavefront curvature
    """

    def __init__(
        self,
        waist,
        # waisty=None,
        focuspos=np.array([0, 0, 0]),
        wavelen=1.064e-6,
        prop_dir=np.array([1, 0, 0]),
        polar_dir=np.array([0, 0, 1]),
        phi=0.0,
        power=None,  # W
        depth=None,
    ) -> None:
        """
        Parameters:
            waist:       Gaussian beam waist at the focu in m
            waisty:      second beam at waist
            focuspos:    Positon of the focusm, Defaults to [0,0,0]
            wavelen:     Wavelength in m. Defaults to 1064e-9
            prop_dir:    Propagation direction
            polar_dir:   Polarization direction (linear)
            power:       Optional, The power in each individual beam in W
            depth:       Optional, Depth of the dipole trap crated by the beam in Hz.
                            NOTE: There are no checks on physical meanings of values

        """

        self.waist = waist
        # self.waist = waist
        self.wavelen = wavelen

        self.k = 2 * pi / self.wavelen

        self.focuspos = focuspos
        self.x0, self.y0, self.z0 = (
            focuspos[0],
            focuspos[1],
            focuspos[2],
        )

        self.zR = pi * self.waist**2 / self.wavelen

        self.prop_dir = prop_dir / np.linalg.norm(prop_dir)
        self.polar_dir = polar_dir / np.linalg.norm(polar_dir)

        self.phi = phi
        self.__setPowerDepth(power, depth)

        if self.waist <= self.wavelen:
            raise warnings.warn("Paraxial approximation no longer valid")
        # print(self.wavelen, self.power, self.depth)

    def __setPowerDepth(self, power, depth):
        # print(power, depth)
        self.depth = depth
        self.power = power
        if power is not None and depth is not None:
            raise ValueError("Give either Power or depth, not both")
        if depth is None and power is not None:
            self.depth = power2freq(power, waist=self.waist, wavelen=self.wavelen)
            self.power = power
        if depth is not None and power is None:
            # depth is linear in power
            self.power = np.abs(
                depth / power2freq(power=1, waist=self.waist, wavelen=self.wavelen)
            )
            self.depth = depth
        # return

    def getWaist(self, pos):
        """
        Calculates the waist in a plane given by the parameter pos
        Parameters:
        -----------
        pos:    np.array or float. if float, it's the distance from the focus along the propagation direction.
        along the propagation direction. If array, it's a point in 3D space

        Returns:
        -----------
        waistz_x, waistz_y    major and minor waists at position pos
        """
        if isinstance(pos, list):
            pos = np.array(pos)
        if isinstance(pos, np.ndarray) and pos.shape[0] == 3:
            z = (self.focuspos - pos) @ self.prop_dir
        else:
            z = pos
        # else:
        # raise ValueError("enter a valid position")

        waist_z = self.waist * np.sqrt(1 + z**2 / self.zR**2)

        return waist_z

    def orderPosition(self, pos):
        """
        A function that (hopefully) takes pos and returns an array of the
        positions along the propagation direction and one along the radial directions.
        """

        if len(pos) == 2:
            X, Y = pos
            Z = 0
        else:
            X, Y, Z = pos

        # distance from Focus
        R = ((X - self.x0) ** 2 + (Y - self.y0) ** 2 + (Z - self.z0) ** 2) ** 0.5

        # distance along the axial direction:
        z_axial = (
            self.prop_dir[0] * (X - self.x0)
            + self.prop_dir[1] * (Y - self.y0)
            + self.prop_dir[2] * (Z - self.z0)
        )

        # Distance along the radial direction
        r = np.sqrt(np.abs(R**2 - z_axial**2))

        # if isinstance(self, EllipticalGaussianBeam):
        #     return z_axial,
        return z_axial, r

    def field(self, pos):
        """
        Calculates the Electric field in a.u. of a gaussian beam with circular
        cross section in far field approximation

        Parameters:
        ----------
        pos:    array or list [X,Y] or [X,Y,Z]
                    coordinates in 3D space
                if pos=[X,Y] field will have shape (len(Y), len(X))
                X,Y should be meshgrids


        Returns:
        ----------
        vector_field:   list o f length 3 containing Ex, Ey, Ez, arrays of the
                        same shape as pos.

        Accounts for wavefront curvature and Gouy phase
        NOTE: only valid in in far field approximation (kz>>1) and far from the
        diffraction limit (kw>>1)
        """

        warnings.warn("E-field calculation doesn't account for power")

        if len(pos) == 2:
            X, Y = pos
            Z = 0
        else:
            X, Y, Z = pos

        # distance from the focus position:
        R = ((X - self.x0) ** 2 + (Y - self.y0) ** 2 + (Z - self.z0) ** 2) ** 0.5

        # distance along the axial direction:
        z_axial = (
            self.prop_dir[0] * (X - self.x0)
            + self.prop_dir[1] * (Y - self.y0)
            + self.prop_dir[2] * (Z - self.z0)
        )

        # Distance along the radial direction
        r = np.sqrt(np.abs(R**2 - z_axial**2))

        # waisty = self.waisty * np.sqrt(1 + z_axial**2 / self.zRy**2)
        # waistx = self.waistx * np.sqrt(1 + z_axial**2 / self.zRx**2)
        waist = self.waist * np.sqrt(1 + z_axial**2 / self.zR**2)

        # wavefront_curv_factor_x = z_axial / (z_axial + self.zRx)  # =1/curvature
        # wavefront_curv_factor_y = z_axial / (z_axial + self.zRy)  # =1/curvature
        wavefront_curv_factor = z_axial / (z_axial + self.zR)  # =1/curvature

        gouy_phase = np.arctan(z_axial / self.zR)

        # return field
        # NOTE: far field approximation

        scalar_field = (
            self.waist
            / waist
            * np.exp(-(r**2) / waist**2)  # Gaussian profile
            * np.exp(
                -1j
                * (
                    X * self.prop_dir[0] + Y * self.prop_dir[1] + Z * self.prop_dir[2]
                )  # k x
                * self.k
                + 1j * gouy_phase
                - 1j * self.phi
            )
            * np.exp(-1j * self.k * r**2 * wavefront_curv_factor / 2)
        )

        return scalar_field

    def getVectorField(self, pos):
        """
        Returns:
        ----------
        vector_field:   list o f length 3 containing Ex, Ey, Ez, arrays of the
                        same shape as pos.
        """
        scalar_field = self.field(pos)
        vector_field = [self.polar_dir[i] * scalar_field for i in range(3)]
        return np.array(vector_field)

    def getIntensity(self, pos):
        """Calculates the intensity of the beam, accounting for power.

        Parameters:
            pos:    array of the form [X,Y,Z]

        """

        E_vec = self.getVectorField(pos)
        E_amplitude_sq = (E_vec.conj() * E_vec).real.sum(axis=0)

        Imax = 2 * self.power / pi / self.waist**2

        return E_amplitude_sq * Imax
        # return E_amplitude_sq

        # intensity = np.zeros_like(pos)


class InterferingBeams:
    """Constructs interference patterns from gaussian beams

    Parameters:
    -------------
    beams:   list or array of beam

    Look at examples\9-interfering_beams.ipynb for usage example

    """

    def __init__(self, beams):
        # if type(beams) != list and not (isinstance(beams, GaussianBeam3D)):
        if not (isinstance(beams, list)) and not (isinstance(beams, GaussianBeam3D)):
            raise Warning("Beams need to be a GaussianBeam3D or a list of them")
        if isinstance(beams, list):
            for element in beams:
                if not isinstance(element, GaussianBeam3D):
                    raise TypeError("Each entry must be an instance of GaussianBeam3D")
                    # elif not (isinstance(beams, GaussianBeam3D)):
                    # for el in beams:
                    #     print(type(el))
                    #     if not (isinstance(el, GaussianBeam3D)) or not (
                    #         isinstance(el, EllipticalGaussianBeam)
                    #     ):
                    raise TypeError("Each entry must be an instance of GaussianBeam3D")
        self.beams = beams

    def getIntensity(self, pos):
        """
        NOTE: there is no time averaging. Beams of wildly different wavelengths
        will still create an interference pattern.

        """
        field = np.zeros_like(self.beams[0].getVectorField(pos))
        for beam in self.beams:
            f = beam.getVectorField(pos)
            Imax = 2 * beam.power / pi / beam.waist**2
            field += f * np.sqrt(Imax)
            print(Imax)

        intensity = (field.conj() * field).real.sum(axis=0)

        return intensity

    def getPotential(self, pos):
        """
        Assumes all the beams are far detuned
        Returns the potential (with sign) of TLS
        """
        field = np.zeros_like(self.beams[0].getVectorField(pos))

        for beam in self.beams:
            f = beam.getVectorField(pos)
            u = power2freq(beam.power, waist=beam.waist, wavelen=beam.wavelen)
            field += f * np.sqrt(np.abs(u))

        pot = (field.conj() * field).real.sum(axis=0) * np.sign(u)

        return pot


class EllipticalGaussianBeam(GaussianBeam3D):
    def __init__(
        self,
        waistx,
        waisty=None,
        focuspos=np.array([0, 0, 0]),
        wavelen=1.064e-6,
        prop_dir=np.array([1, 0, 0]),
        polar_dir=np.array([0, 0, 1]),
        phi=0.0,
        power=None,  # W
        depth=None,
        angle=0,  # deg
        wx_axis=np.array([0, 1, 0]),
    ) -> None:
        """
        Most of the parameters are the same as on GaussiaBwam3D
        Parameters:
        ---------------
            waistx:      Gaussian beam waist at the focus
            waisty:      beam waist
            focuspos:    Positon of the focusm, Defaults to [0,0,0]
            wavelen:     Wavelength in m. Defaults to 1064e-9
            prop_dir:    Propagation direction
            polar_dir:   Polarization direction (linear)
            power:       Optional, The power in each individual beam in W
            depth:       Optional, Depth of the dipole trap crated by the beam in Hz.
                            NOTE: There are no checks on physical meanings of values
            wx_axis:        direction of the waistx. Needs to be a 3D vector in space

        """
        self.waisty = waisty
        self.waistx = waistx
        self.waist_geom = np.sqrt(self.waistx * self.waisty)
        self.waist = self.waist_geom
        self.angle = angle
        self.wx_axis = wx_axis / np.linalg.norm(wx_axis)

        super().__init__(
            self.waist_geom, focuspos, wavelen, prop_dir, polar_dir, phi, power, depth
        )

        # Two ciruclar gaussian beams of waists waistx and waisty (can be useful)
        self.beamx = GaussianBeam3D(
            waistx, focuspos, wavelen, prop_dir, polar_dir, phi, power=self.power
        )
        self.beamy = GaussianBeam3D(
            waisty, focuspos, wavelen, prop_dir, polar_dir, phi, power=self.power
        )

        if self.waist_geom <= self.wavelen:
            raise warnings.warn("Paraxial approximation no longer valid")

        self.zRx = pi * self.waistx**2 / self.wavelen
        self.zRy = pi * self.waisty**2 / self.wavelen

        if np.abs(self.prop_dir @ self.wx_axis) > 1e-15:
            raise ValueError("axis needs to be perpendicular to propagation directoin")

    def getWaist(self, axial):
        """
        Parameters:
        -------------
        axial:          array or float, distance from focus along propagation direction

        """
        waistx_z = self.beamx.getWaist(axial)
        waisty_z = self.beamy.getWaist(axial)

        return waistx_z, waisty_z

    def getScalarField(self, pos):
        """
        Calculates scalar field
        """
        return self.field(pos)

    def beamCoords(self, pos):
        """
        A function that that converts position in cartesian 3D space
        to beam coordinates (distance along the propagation axis,
        distance along minor and major axis)


        Returns
        -----------
        axial:      z, distance from the focus along the propagation axis
        axis_x:     x, distance from the propagation axis, projected on the waistx direction
        axis_y:     y, distance from the propagation axis, projected on the waisty direction


        note: axis_x**2 + axis_y**2 = r**2
        """

        if len(pos) == 2:
            X, Y = pos
            Z = 0
        else:
            X, Y, Z = pos

        ######## distance from Focus

        Rx = X - self.x0
        Ry = Y - self.y0
        Rz = Z - self.z0
        Rvec = [Rx, Ry, Rz]

        R = list(map(lambda x: x**2, Rvec))
        R = sum(R) ** 0.5

        ######## distance along the axial direction:
        axial = sum([self.prop_dir[i] * Rvec[i] for i in range(3)])

        ######## Distance along the radial direction
        rvec = [Rvec[i] - self.prop_dir[i] * axial for i in range(3)]

        ########x and y coordinates
        axis_x = sum([rvec[i] * self.wx_axis[i] for i in range(3)])
        axis_y = np.sqrt(np.abs(sum(list(map(lambda x: x**2, rvec))) - axis_x**2))
        return axial, axis_x, axis_y  # , r

    def _1DGauss(self, x, z, whichwaist):
        """
        calculates the 1D gaussian distribution of the Electric field along
        the direction of waist x or waist y

        x:      radial coordinate
        z:      axial coordinate
        whichwaist: 0 for waistx, 1 for waisty

        """

        if whichwaist == 0:
            w0 = self.waistx
            zR = self.zRx
        elif whichwaist == 1:
            w0 = self.waisty
            zR = self.zRy
        else:
            raise ValueError

        q = np.array(zR - 1j * z)
        w = self.getWaist(z)[whichwaist]
        gouy = self.getGouy(z)[whichwaist]

        return (
            np.sqrt(w0 / w)
            * np.exp(-1j * self.k * z / 2 * (1 + x**2 / q.conj() / q))
            * np.exp(1j * gouy - 1j * self.phi)
            * np.exp(-(x**2) / w**2)
        )

    def field(self, pos):
        """
        Calculates the field along the x and y and multiplies them

        Parameters:
        --------------
        pos:        list or array, (meshgrid) of shape (3,)
        """

        z, x, y = self.beamCoords(pos)
        fieldx = self._1DGauss(x, z, 0)
        fieldy = self._1DGauss(y, z, 1)
        # prefactor = np.sqrt(2 * self.power * pi) * self.waist_geom / self.wavelen
        # prefactor = 1
        return fieldx * fieldy

    def getVectorField(self, pos):
        """
        Separates the components of the scalar field in the polarization components.


        Parameters:
        --------------
        pos:        list or array, (meshgrid) of shape (3,)


        Returns:
        vector_field      np.array of shape (3,...)

        TODO: should, in principle be more subtle if the polarization has a component
        along the propagation direction?

        """
        scalar_field = self.field(pos)
        vector_field = [self.polar_dir[i] * scalar_field for i in range(3)]
        return np.array(vector_field)

    def getGouy(self, z):
        """
        Computes the gouy phase.


        Parameters:
        -------------
        axial:          array or float, distance from focus along propagation direction

        """
        return np.arctan(z / self.zRx), np.arctan(z / self.zRy)

    def qParameter(self, z):
        """
        Computes the gaussian beam complex parameter.


        Parameters:
        -------------
        z:          array or float, distance from focus along propagation direction


        Returns:
        -------------
        Tuple of the complex parameter along x and y

        """
        return z - 1j * self.zRx, z - 1j * self.zRy


def intensity(
    power: np.ndarray,
    radius: float,
    radial_pos: float = 0,
):
    """Returns the intensity of a gaussian beam given the radius and the distance
    from the axis.

    Args:
    --
        power:      Beam Power in W
        radius:     1/e^2 radius, for an elliptical beam give the geometric mean.At focus, it's the waist
        radial_pos: radial distance from the center axis of the beam. Defaults to 0.
        omega_transition: angular fequency of the non-shifted transition

    Returns:
    --
        I: Intensity

    """
    I_max = 2 * power / pi / radius**2
    return I_max * np.exp(-2 * radial_pos**2 / radius**2)


def power2U(
    power,
    waist,
    wavelen=1064e-9,
    omega_transition=omega0,
    Er=None,
):
    """Returns the trap depth for a single beam given power and waist
    Parameters
    =========
    power               Beam Power in W
    waist               waist, for an elliptical beam give the geometric mean
    omega_transition    angular fequency of the non-shifted transition
    Er                  if None returns U in SI units
                        if 'Hz' return in 2pi Hz
                        if a number returns U in units of recoil energy.
    """
    omega = 2 * pi / wavelen
    U_SI = (
        -3
        * pi
        * c**2
        / (2 * omega_transition**3)
        * (Gamma / (omega_transition - omega) + Gamma / (omega_transition + omega))
        * intensity(
            power,
            0,
            waist,
            omega_transition=omega_transition,
        )
    )
    if Er is None:
        return U_SI
    elif Er == "Hz":
        return U_SI / hplanck
    else:
        return U_SI / Er
